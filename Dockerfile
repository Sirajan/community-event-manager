#
# PHP Dependencies
#
FROM composer:latest as vendor

# COPY database/ database/

COPY composer.json composer.json
COPY composer.lock composer.lock

RUN composer install \
  --ignore-platform-reqs \
  --no-interaction \
  --no-plugins \
  --no-scripts \
  --prefer-dist

#
# Frontend
#
FROM node:lts-slim as frontend

ARG APP_ENV
ENV APP_ENV=$APP_ENV

RUN mkdir -p /app/public

COPY package.json webpack.config.js yarn.lock /app/
COPY assets/ /app/assets/

WORKDIR /app

RUN yarn install && yarn encore $APP_ENV

#
# Application
#
FROM php:apache-stretch

ARG S3_SECRET
ARG S3_KEY
ARG S3_REGION

ENV PROJECT_DIR /var/www/project
ENV S3_REGION=$S3_REGION
ENV S3_KEY=$S3_KEY
ENV S3_SECRET=$S3_SECRET

COPY . ${PROJECT_DIR}

COPY --from=vendor /app/vendor/ ${PROJECT_DIR}/vendor/
COPY --from=frontend /app/public/build/ ${PROJECT_DIR}/public/build
# COPY --from=frontend /app/public/js/ ${PROJECT_DIR}/public/js/
# COPY --from=frontend /app/public/css/ ${PROJECT_DIR}/public/css/
# COPY --from=frontend /app/public ${PROJECT_DIR}/mix-manifest.json

RUN apt-get update && apt-get install --no-install-recommends --assume-yes --quiet ca-certificates curl libzip-dev libpq-dev git libicu-dev libpng-dev libjpeg62-turbo-dev \
  && docker-php-ext-configure intl \
  && docker-php-ext-install zip intl pdo pdo_pgsql pgsql gd \
  && apt-get clean && rm -rf /var/lib/apt/lists/* \
  && curl -Lsf 'https://storage.googleapis.com/golang/go1.8.3.linux-amd64.tar.gz' | tar -C '/usr/local' -xvzf - \
  && sed -i "s|DocumentRoot /var/www/html|DocumentRoot ${PROJECT_DIR}/public|" /etc/apache2/sites-available/000-default.conf \
  && a2enmod rewrite \
  && echo "ServerName localhost" | tee /etc/apache2/conf-available/servername.conf && a2enconf servername \
  && chown www-data:www-data -R ${PROJECT_DIR}

CMD sed -i "s/80/$PORT/g" /etc/apache2/sites-enabled/000-default.conf /etc/apache2/ports.conf && docker-php-entrypoint apache2-foreground
